# 주의사항  
이 저장소의 master 가지(branch)는 조원들 간에 합의가 이루어진 내용만을 담는 용도의 가지입니다.
각 개인별 가지는 아래를 참고하시기 바랍니다.  

# 개인별 가지의 이름은 각자의 Bitbucket username 그대로 지어주시기 바랍니다.  

# 저장소 내부 구조(.git 폴더 및 .gitignore 파일은 보이더라도 건들면 안됩니다.)  
## show 폴더 : 발표자료 저장  
## reports 폴더 : 여기에 보고서 작업파일 저장  
### 개인별 가지 사용법
1. [Git for Windows](http://msysgit.github.io/) 다운로드 후 설치(Git Bash도 반드시 설치)  
2. 적당한 곳에서 우클릭 -> Git Bash 실행
3. Git Bash 명령창에서, git clone https://<ID\>@bitbucket.org/ku_sociology_group4/works/ 의 <ID\>를 자신의 ID로 바꿔 입력하고 Enter  
4. [Texture](https://github.com/substance/texture/releases/download/v1.0.0-preview.1/Texture.Setup.1.0.0-preview.1-win.exe) 다운로드 후 설치  
5. 3에서 생성된 works 폴더로 들어가서 우클릭 -> Git Bash 실행  
6. Git Bash 명령창에서 git checkout -b <자신의 가지\> 의 <자신의 가지\>를 자신의 ID로 바꿔 입력하고 Enter  
7. Texture에서 reports 폴더 불러오기
8. 작업내용 저장
9. 4번의 works 폴더로 돌아와서 우클릭 -> Git Bash 실행
10. Git Bash 명령창에서 git add * 를 입력하고 Enter
11. Git Bash 명령창에서 git commit -m "수정한 부분에 대한 설명" 을 입력하고 Enter
12. Git Bash 명령창에서 git push origin <자신의 가지\> 의 <자신의 가지\>를 자신의 ID로 바꿔 입력하고 Enter  
